var server = require('ws').Server;
const fs = require('fs');
//let jsonData = require('./test_output.json');
var s = new server({port:5001});

console.log("started.");

s.on('connection', function(ws){
    //ws.send("RESPONSE FROM SERVER 1");
    ws.on('message', function(message){
        console.log("Received: "+message);

        //ws.send("RESPONSE FROM SERVER 2");

        if(message === "init") {
            console.log("Message was init");
            let rawdata = fs.readFileSync('test_output.json');
            let jsonData = JSON.parse(rawdata);
            let sJsonData = JSON.stringify(jsonData);
            console.log(sJsonData);
            ws.send(sJsonData);
        }
    })
});